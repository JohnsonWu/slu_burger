# SLU_Burger

Burger HTML

# 參考影片

https://www.youtube.com/watch?v=3ypel9_VtmU

# 步驟

1. 請先安裝 vue cli
* https://cli.vuejs.org/zh/guide/installation.html

2. 進到 burger 資料夾後執行

* npm run serve \*註: 需安裝 node.js(https://nodejs.org/en/download/)

3. 在預覽器中輸入 http://localhost:8080

# src 資料夾底下是主要我們會碰到的檔案

# 請注意

1. 請試著使用 TypeScript 來寫

2. 我們使用的是 Vue 3 不是 Vue 2

3. 請先不要使用 BootStrap

# VSCode Extensions
* 請參考 https://www.youtube.com/watch?v=7qfMmCN4Uwk

1. Eslint
* Eslint 是一個檢查你寫的 code 好不好的一個工具
2. Vetur
3. Prettier
* Prettier 是一個可以幫你自動排版的工具